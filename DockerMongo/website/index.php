<html>
    
    <head>
    <style>
        <?php include './styles.css'; ?>
    </style>
        <title>CIS 322 REST-api demo: Laptop list</title>
    </head>

    <body>
    <h1>API Consumer for Brevet Calculator</h1>
        <div class="grid">
            <div class="col1">
                
                <div>
                    <div class="defaul">
                        <h2>Default Endpoints</h2>
                        <form class="endpoint-btn-grid" method="get">
                            <input class="end-btn" type="submit" name="listAll" id="listAll" value="/listAll" />
                            <input class="end-btn" type="submit" name="listOpenOnly" id="listOpenOnly" value="/listOpenOnly" />
                            <input class="end-btn" type="submit" name="listCloseOnly" id="listCloseOnly" value="/listCloseOnly" />
                            </form>
                    </div>
                    <div class="csv">
                        <h2>CSV Endpoints</h2>
                        <form class="endpoint-btn-grid" method="get">
                            <input class="end-btn" type="submit" name="listAllcsv" id="listAllcsv" value="/listAll/csv" />
                            <input class="end-btn" type="submit" name="listOpenOnlycsv" id="listOpenOnlycsv" value="/listOpenOnly/csv" />
                            <input class="end-btn" type="submit" name="listCloseOnlycsv" id="listCloseOnlycsv" value="/listCloseOnly/csv" />
</form>
                        <h3>Top Input fields olnly accept integers. It is up to other consumers to ensure user input is correct.</h3>
                        <div class="endpoint-top-grid">
                            <div>
                            <form method="GET">
                                <input class="top-btn" type="submit" name="listOpenOnlycsvTop" id="listOpenOnlycsvTop" value="/listOpenOnly/csv?top" />
                                <input class="top-field" type="text" name="top" id="top" value="" /> 
                            </form>
                        </div>
                        <div>
                            <form method="GET">
                                <input class="top-btn" type="submit" name="listCloseOnlycsvTop" id="listCloseOnlycsvTop" value="/listCloseOnly/csv?top" />
                                <input class="top-field" type="text" name="top" id="top" value="" /> 
                            </form>
                        </div>
                        </div>
                    </div>
                    <div class="json">
                        <h2>JSON Endpoints</h2>
                        <form class="endpoint-btn-grid" method="get">
                            <input class="end-btn" type="submit" name="listAlljson" id="listAlljson" value="/listAll/json" />
                            <input class="end-btn" type="submit" name="listOpenOnlyjson" id="listOpenOnlyjson" value="/listOpenOnly/json" />
                            <input class="end-btn" type="submit" name="listCloseOnlyjson" id="listCloseOnlyjson" value="/listCloseOnly/json" />
</form>
                        <h3>Top Input fields olnly accept integers. It is up to other consumers to ensure user input is correct.</h3>
                        <div class="endpoint-top-grid">
                        <div>
                            <form method="GET">
                                <input class="top-btn" type="submit" name="listOpenOnlyjsonTop" id="listOpenOnlyjsonTop" value="/listOpenOnly/json?top" />
                                <input class="top-field" type="text" name="top" id="top" value="" /> 
                            </form>
</div>
<div>
                            <form method="GET">
                                <input class="top-btn" type="submit" name="listCloseOnlyjsonTop" id="listCloseOnlyjsonTop" value="/listCloseOnly/json?top" />
                                <input class="top-field" type="text" name="top" id="top" value="" /> 
                            </form>
</div>
                        </div>
                    </div>
</div>
            </div>
            <div class="col2">
        <ul>
            <?php
            if(array_key_exists('listAll',$_GET)) {
                listAll();
            } if(array_key_exists('listOpenOnly',$_GET)) {
                listOpenOnly();
            } if(array_key_exists('listCloseOnly',$_GET)) {
                listCloseOnly();
            } if(array_key_exists('listAllcsv',$_GET)) {
                listAllcsv();
            } if(array_key_exists('listCloseOnlycsv',$_GET)) {
                listCloseOnlycsv();
            } if(array_key_exists('listOpenOnlycsv',$_GET)) {
                listOpenOnlycsv();
            } if(array_key_exists('listAlljson',$_GET)) {
                listAlljson();
            } if(array_key_exists('listOpenOnlyjson',$_GET)) {
                listOpenOnlyjson();
            } if(array_key_exists('listCloseOnlyjson',$_GET)) {
                listCloseOnlyjson();
            } if(array_key_exists('listCloseOnlyjsonTop',$_GET)) {
                listCloseOnlyjsonTop();
            } if(array_key_exists('listOpenOnlyjsonTop',$_GET)) {
                listOpenOnlyjsonTop();
            } if(array_key_exists('listCloseOnlycsvTop',$_GET)) {
                listCloseOnlycsvTop();
            } if(array_key_exists('listOpenOnlycsvTop',$_GET)) {
                listOpenOnlycsvTop();
            }
            

            function listAll(){
                $json = file_get_contents('http://calculator/listAll');
                $obj = json_decode($json);
                foreach ($obj as $pair) {
                    echo "<li>Open: $pair->open Close: $pair->close</li>";
                }
            }
            function listOpenOnly(){
                $json = file_get_contents('http://calculator/listOpenOnly');
                $obj = json_decode($json);
                foreach ($obj as $pair) {
                    echo "<li>Open: $pair->open</li>";
                }
            }
            function listCloseOnly(){
                $json = file_get_contents('http://calculator/listCloseOnly');
                $obj = json_decode($json);
                foreach ($obj as $pair) {
                    echo "<li>Close: $pair->close</li>";
                }
            }
            function listAllcsv(){
                $json = file_get_contents('http://calculator/listAll/csv');
                $obj = json_decode($json);
                echo "<li>Open Times: $obj[0] </li>";
                echo "<li>Close Times: $obj[1] </li>";
            }
            function listOpenOnlycsv(){
                $json = file_get_contents('http://calculator/listOpenOnly/csv');
                echo "<li>Open: $json </li>";
            }
            function listCloseOnlycsv(){
                $json = file_get_contents('http://calculator/listCloseOnly/csv');
                echo "<li>Close: $json </li>";
            }
            function listAlljson(){
                $json = file_get_contents('http://calculator/listAll/json');
                $obj = json_decode($json);
                foreach ($obj as $pair) {
                    echo "<li>Open: $pair->open Close: $pair->close</li>";
                }
            }
            function listOpenOnlyjson(){
                $json = file_get_contents('http://calculator/listOpenOnly/json');
                $obj = json_decode($json);
                foreach ($obj as $pair) {
                    echo "<li>Open: $pair->open</li>";
                }
            }
            function listCloseOnlyjson(){
                $json = file_get_contents('http://calculator/listCloseOnly/json');
                $obj = json_decode($json);
                foreach ($obj as $pair) {
                    echo "<li>Close: $pair->close</li>";
                }
            }
            function listCloseOnlyjsonTop(){
                $url = 'http://calculator/listCloseOnly/json?top=';
                $url .= $_GET['top'];
                $json = file_get_contents($url);
                $obj = json_decode($json);
                foreach ($obj as $pair) {
                    echo "<li>Close: $pair->close</li>";
                }
            }
            function listOpenOnlyjsonTop(){
                $url = 'http://calculator/listOpenOnly/json?top=';
                $url .= $_GET['top'];
                $json = file_get_contents($url);
                $obj = json_decode($json);
                foreach ($obj as $pair) {
                    echo "<li>Open: $pair->open</li>";
                }
            }
            function listCloseOnlycsvTop(){
                $url = 'http://calculator/listCloseOnly/csv?top=';
                $url .= $_GET['top'];
                $json = file_get_contents($url);
                echo "<li>Close: $json</li>";
            }
            function listOpenOnlycsvTop(){
                $url = 'http://calculator/listOpenOnly/csv?top=';
                $url .= $_GET['top'];
                $json = file_get_contents($url);
                echo "<li>Open: $json</li>";
            }
            ?>
        </ul>
        </div>
        </div>
    </body>
</html>
