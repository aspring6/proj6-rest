"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

def error_check(dist, brevet_dist_km):
    """Checks for invalid input, and returns true if input distance was invalid"""
    if(dist < 0 or dist > brevet_dist_km*1.2):
        return True
    return False

def round_minutes(non_int):
    """Need to adjust rounding so minutes only round up if they are > 0.5"""
    whole = non_int // 1
    if((non_int - whole) > 0.5):
        return round(non_int)
    return(whole)

def adjust_time(MAX_TIME, dist, control_time):
    add_hours = dist//MAX_TIME
    add_minutes = round_minutes(((dist % MAX_TIME)/MAX_TIME)*60)
    control_time = control_time.shift(hours=+add_hours, minutes=+add_minutes)
    return control_time


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    control_time = arrow.get(brevet_start_time)
    dist = control_dist_km

    if(error_check(dist, brevet_dist_km)):
        return -1

    #---- ODDITIES ----
    #if dist > brevet distance, set it to the brevet distance
    if(dist == 0):
        #if distance = 0, just return the converted time (that was passed in)
        return control_time.isoformat()
    if(dist > brevet_dist_km):
        dist = brevet_dist_km
    #---- END ODDITIES ----

    while(dist >= 0):
        if(dist <= 200):
            control_time = adjust_time(34, dist, control_time)
            #This is the last interval, so break while loop
            break
        elif(dist <= 400):
            interval = dist - 200
            control_time = adjust_time(32, interval, control_time)
            dist -= interval
        elif(dist <= 600):
            interval = dist - 400
            control_time = adjust_time(30, interval, control_time)
            dist -= interval
        elif(dist <= 1000):
            interval = dist - 600
            control_time = adjust_time(28, interval, control_time)
            dist -= interval
        elif(dist <= 1300):
            interval = dist - 1000
            control_time = adjust_time(26, interval, control_time)
            dist -= interval
        else:
            return NULL
        
    #NEED TO SHIFT 8 HOURS: for MomentJS conversion
    return control_time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    control_time = arrow.get(brevet_start_time)
    dist = control_dist_km

    if(error_check(dist, brevet_dist_km)):
        return -1

    #---- ODDITIES ----
    #if dist > brevet distance, set it to the brevet distance
    if(dist == 0):
        #if distance = 0, just return the converted time (that was passed in) + 1hr
        return control_time.shift(hours=+1).isoformat()
    #relaxed first 60km condition: 20km/hr with +1hr time shift
    if(dist < 60):
        control_time = adjust_time(20, dist, control_time)
        return control_time.shift(hours=+1).isoformat()
    if(dist > brevet_dist_km):
        dist = brevet_dist_km
    #---- END ODDITIES ----

    while(dist >= 0):
        if(dist <= 600):
            control_time = adjust_time(15, dist, control_time)
            break
        elif(dist <= 1000):
            interval = dist - 600
            control_time = adjust_time(11.428, interval, control_time)
            dist -= interval
        elif(dist <= 1300):
            interval = dist - 1000
            control_time = adjust_time(13.333, interval, control_time)
            dist -= interval
        else:
            return NULL
        
    #NEED TO SHIFT 8 HOURS: for MomentJS conversion
    return control_time.isoformat()
