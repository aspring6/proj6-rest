"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
import os
from flask import Flask, redirect, url_for, request, render_template
from flask_restful import Resource, Api
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from datetime import datetime


import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
###
# API
###

api = Api(app)

###
# ################################# BASIC #################################
###
class listAll(Resource):
    def get(self):
        _items = db.tododb.find()
        dates = [item for item in _items]
        for document in dates:
            del document['_id']
        print(dates)
        return dates

class listOpenOnly(Resource):
    def get(self):
        _items = db.tododb.find()
        print("ITEMS:", _items)
        dates = [item for item in _items]
        print("DATES:",dates)
        for document in dates:
            del document['_id']
            del document['close']
        print(dates)
        return dates

class listCloseOnly(Resource):
    def get(self):
        _items = db.tododb.find()
        print("ITEMS:", _items)
        dates = [item for item in _items]
        print("DATES:",dates)
        for document in dates:
            del document['_id']
            del document['open']
        print(dates)
        return dates

###
# ################################# CSV #################################
###

class listAllcsv(Resource):
    def get(self):
        _items = db.tododb.find()
        dates = [item for item in _items]
        open = ""
        close = ""
        for document in dates:
            open += document['open']
            open += ","
            close += document['close']
            close += ","
        close = close[:-1]
        open = open[:-1]
        return [open, close]

class listOpenOnlycsv(Resource):
    def get(self):
        _items = db.tododb.find()
        dates = [item for item in _items]
        open = ""
        if 'top' in request.args:
            top = int(request.args['top'])
            #Create date objects
            obj = []
            for document in dates:
                obj.append(datetime.strptime(document['open'], '%a %m/%d %H:%M'))
            #Sort
            obj.sort()
            sorted = []
            #Reformat to match our original date time format
            for t in obj:
                newTime = t.strftime('%a %m/%d %H:%M').lstrip("0").replace(" 0", " ")
                newTime = newTime.lstrip("/0").replace("/0", "/")
                sorted.append(newTime)
            
            csv = ""
            if(top > len(obj)):
                top = len(obj)
            while(top != 0):
                csv += sorted[-top]
                csv += ","
                top -= 1
            csv = csv[:-1]
            return csv
        for document in dates:
            open += document['open']
            open += ","
        open = open[:-1]
        return open

class listCloseOnlycsv(Resource):
    def get(self):
        _items = db.tododb.find()
        dates = [item for item in _items]
        close = ""
        if 'top' in request.args:
            top = int(request.args['top'])
            #Create date objects
            obj = []
            for document in dates:
                obj.append(datetime.strptime(document['close'], '%a %m/%d %H:%M'))
            #Sort
            obj.sort()
            sorted = []
            #Reformat to match our original date time format
            for t in obj:
                newTime = t.strftime('%a %m/%d %H:%M').lstrip("0").replace(" 0", " ")
                newTime = newTime.lstrip("/0").replace("/0", "/")
                sorted.append(newTime)
            
            closeCSV = ""
            if(top > len(obj)):
                top = len(obj)
            while(top != 0):
                closeCSV += sorted[-top]
                closeCSV += ","
                top -= 1
            closeCSV = closeCSV[:-1]
            return closeCSV
        for document in dates:
            close += document['close']
            close += ","
        close = close[:-1]
        return close

###
# ################################# JSON #################################
###
class listAll_json(Resource):
    def get(self):
        _items = db.tododb.find()
        dates = [item for item in _items]
        for document in dates:
            del document['_id']
        print(dates)
        return dates

class listOpenOnly_json(Resource):
    def get(self):
        _items = db.tododb.find()
        dates = [item for item in _items]
        if 'top' in request.args:
            top = int(request.args['top'])
            #Create date objects
            obj = []
            for document in dates:
                obj.append(datetime.strptime(document['open'], '%a %m/%d %H:%M'))
            #Sort
            obj.sort()
            sorted = []
            #Reformat to match our original date time format
            for t in obj:
                newTime = t.strftime('%a %m/%d %H:%M').lstrip("0").replace(" 0", " ")
                newTime = newTime.lstrip("/0").replace("/0", "/")
                sorted.append(newTime)
            
            json = []
            if(top > len(obj)):
                top = len(obj)
            while(top != 0):
                doc = {
                    "open": sorted[-top]
                }
                json.append(doc)
                top -= 1
            return json
        for document in dates:
            del document['_id']
            del document['close']
        print(dates)
        return dates

class listCloseOnly_json(Resource):
    def get(self):
        _items = db.tododb.find()
        dates = [item for item in _items]
        if 'top' in request.args:
            top = int(request.args['top'])
            #Create date objects
            obj = []
            for document in dates:
                obj.append(datetime.strptime(document['close'], '%a %m/%d %H:%M'))
            #Sort
            obj.sort()
            sorted = []
            #Reformat to match our original date time format
            for t in obj:
                newTime = t.strftime('%a %m/%d %H:%M').lstrip("0").replace(" 0", " ")
                newTime = newTime.lstrip("/0").replace("/0", "/")
                sorted.append(newTime)
            
            json = []
            if(top > len(obj)):
                top = len(obj)
            while(top != 0):
                doc = {
                    "close": sorted[-top]
                }
                json.append(doc)
                top -= 1
            return json
        for document in dates:
            del document['_id']
            del document['open']
        print(dates)
        return dates

# Create routes
# Another way, without decorators
api.add_resource(listAll, '/listAll')
api.add_resource(listOpenOnly, '/listOpenOnly')
api.add_resource(listCloseOnly, '/listCloseOnly')
api.add_resource(listAllcsv, '/listAll/csv')
api.add_resource(listOpenOnlycsv, '/listOpenOnly/csv')
api.add_resource(listCloseOnlycsv, '/listCloseOnly/csv')
api.add_resource(listAll_json, '/listAll/json')
api.add_resource(listOpenOnly_json, '/listOpenOnly/json')
api.add_resource(listCloseOnly_json, '/listCloseOnly/json')


###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', 200, type=float)
    begin_date = request.args.get('begin_date', '', type=str)#FIXME default
    begin_time = request.args.get('begin_time', '', type=str)#FIXME default
    formatted_date = begin_date + ' ' + begin_time
    fmt = "YYYY-MM-DD HH:mm"
    start = arrow.get(formatted_date, fmt).isoformat()
    #start = start.shift(hours=+1.5)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, distance, start)
    close_time = acp_times.close_time(km, distance, start)
    #start.format(fmt) -> string
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############
#
# MongoDB
#
#############

@app.route('/_show_times', methods=['GET'])
def times():
    _items = db.tododb.find()
    dates = [item for item in _items]
    for document in dates:
        document['_id'] = str(document['_id'])
    print(dates)
    return flask.jsonify(dates=dates)

@app.route('/_insert_times', methods=['POST'])
def new():
    db.tododb.drop()
    open = request.form['open']
    close = request.form['close']
    open = open.split(",")
    close = close.split(",")
    #Remove first comma separated value (empty).
    #Doing this on the backend through string slicing is faster than an if else/flag
    #in the front end
    open = open[1:]
    close = close[1:]

    print(open)
    for i in range(len(open)):
        item_doc = {
            'open': open[i],
            'close': close[i]
        }
        print(item_doc)
        db.tododb.insert_one(item_doc)

    return "hi"

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
